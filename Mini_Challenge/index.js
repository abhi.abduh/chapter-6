const { render } = require("ejs");
const express = require("express");
const app = express();
const port = 3000;
const { UserGame, UserBiodata, UserHistory } = require("./models");

app.set("view engine", "ejs");
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(express.static("views"));

app.get("/", (req, res) => {
  UserGame.findAll()
    .then((result) => {
      res.status(200).render("index", { result });
    })
    .catch((err) => {
      res.status(500).send(err);
    });
});

app.get("/userCreate", (req, res) => {
  res.status(200).render("create");
});

app.post("/createUser", async (req, res) => {
  const { username, password, name, description, company } = req.body;
  try {
    const userGame = await UserGame.create({
      username,
      password,
    });
    const userBiodata = await UserBiodata.create({
      name,
      company,
      description,
      user_game_id: userGame.id,
    });
    res.redirect("/");
  } catch (err) {
    res.send(err);
  }
});

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});

// Terminal execution:
//  npx sequelize model:generate --name UserGame --attributes username:string,password:string
/// npx sequelize model:generate --name UserBiodata --attributes name:string,company:string,description:string,user_game_id:integer
// npx sequelize model:generate --name UserHistory --attributes game_name:string,score:integer,duration:integer,user_game_id:integer
