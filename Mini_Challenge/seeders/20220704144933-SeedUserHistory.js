"use strict";

module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.bulkInsert(
      "UserHistories",
      [
        {
          // id: 1,
          game_name: "Guardian of the Galaxy",
          score: 88,
          duration: 88888,
          user_game_id: 1,
          createdAt: "2022-06-29T14:37:15.304Z",
          updatedAt: "2022-06-29T14:37:15.304Z",
        },
        {
          // id: 2,
          game_name: "God of War",
          score: 58,
          duration: 94,
          user_game_id: 2,
          createdAt: "2022-06-29T14:37:15.304Z",
          updatedAt: "2022-06-29T14:37:15.304Z",
        },
        {
          // id: 3,
          game_name: "Dota 2",
          score: 34,
          duration: 60,
          user_game_id: 3,
          createdAt: "2022-06-29T14:37:15.304Z",
          updatedAt: "2022-06-29T14:37:15.304Z",
        },
      ],
      {}
    );
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  },
};
