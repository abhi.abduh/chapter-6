"use strict";

module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.bulkInsert(
      "UserBiodata",
      [
        {
          // id: 3,
          name: "Johntor",
          company: "Tokopedia",
          description: "eCommerce",
          user_game_id: 1,
          createdAt: "2022-06-29T14:37:15.304Z",
          updatedAt: "2022-06-29T14:37:15.304Z",
        },
        {
          // id: 3,
          name: "Johndanjun",
          company: "KlinikGo",
          description: "HealthTech",
          user_game_id: 2,
          createdAt: "2022-06-29T14:37:15.304Z",
          updatedAt: "2022-06-29T14:37:15.304Z",
        },
        {
          // id: 3,
          name: "Nadzeri",
          company: "HaloDoc",
          description: "HealthConsultant",
          user_game_id: 3,
          createdAt: "2022-06-29T14:37:15.304Z",
          updatedAt: "2022-06-29T14:37:15.304Z",
        },
      ],
      {}
    );
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  },
};
